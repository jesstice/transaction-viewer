import moment from 'moment';

import { addCommaToNumber } from './addCommaToNumber';

export const calculateBalance = (total, amount) => {
  return Math.round((total + amount) * 100) / 100;
};

export const calculateAndFormatTransactions = data => {
  // start from oldest transactions to determine balance
  const formatted = data.reduceRight((acc, val) => {
    const transaction = val.node;
    const transactionAmount = parseFloat(transaction.Amount);

    transaction.Balance = calculateBalance(acc.totalBalance, transactionAmount);
    transaction.Balance = addCommaToNumber(transaction.Balance);

    transaction.Date = moment(transaction.Date, 'YYYY-MM-DD').format('MMM Do, YYYY');

    acc.transactions.unshift(transaction);
    acc.totalBalance += transactionAmount;
    return acc;
  }, {
    transactions: [],
    totalBalance: 0
  });

  formatted.totalBalance = addCommaToNumber(formatted.totalBalance);
  return formatted;
};
