export const addCommaToNumber = number => {
  // toLocaleString is not supported by some browsers, might be better
  // to look into a library
  return number.toLocaleString(undefined, { minimumFractionDigits: 2 });
};