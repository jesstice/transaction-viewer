import React from 'react';
import PropTypes from 'prop-types';

// React Table components do not accept styleClass, inline styles required
export const ColumnHeader = ({ value, style }) => (
  <div style={style}>{value}</div>
);

ColumnHeader.propTypes = {
  value: PropTypes.string.isRequired
};

export const ColumnCell = ({ value, style }) => (
  <div style={style}>{value}</div>
);

ColumnCell.propTypes = {
  value: PropTypes.string.isRequired
};