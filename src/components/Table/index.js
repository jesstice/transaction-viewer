import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import { setColumnsProps, setRowColours } from './utils';

const Table = ({ rows, totalBalance }) => {
  return (
    <ReactTable
      columns={setColumnsProps(totalBalance)}
      data={rows}
      pageSize={40}
      showPaginationBottom={false}
      getTheadGroupProps={() => ({ style: { backgroundColor: 'white' } })}
      getTdProps={(state, rowInfo, column) => {
        if (!rowInfo) return {};
        return {
          style: {
            color: setRowColours(rowInfo.row, column),
            backgroundColor: 'rgb(248, 248, 248)',
          }
        };
      }}
    />
  )
};

Table.propTypes = {
  rows: PropTypes.array.isRequired,
  totalBalance: PropTypes.string.isRequired
};

export default Table;