import React from 'react';

import { ColumnHeader, ColumnCell } from './Column';

// Need to set default styles in the column props as
// passing to component does not style container element
const headerComponentStyle = {
  padding: '12px 20px',
  backgroundColor: 'white',
  color: '#098B8C',
  fontSize: '13px',
  fontWeight: '500',
  textAlign: 'left'
};

const columnComponentStyle = {
  padding: '14px 20px',
  fontSize: '11px'
};

// React table has specific way of setting the column and cell components
export const setColumnsProps = (totalBalance) => (
  [{
    Header: () => (<ColumnHeader value="Date"/>),
    accessor: 'Date',
    Cell: row => (<ColumnCell value={row.value}/>),
    minWidth: 30,
    headerStyle: headerComponentStyle,
    style: columnComponentStyle
  }, {
    Header: () => (<ColumnHeader value="Company"/>),
    accessor: 'Company',
    Cell: row => (<ColumnCell value={row.value} />),
    headerStyle: headerComponentStyle,
    style: { ...columnComponentStyle, fontWeight: '500' }
  }, {
    Header: () => (<ColumnHeader value="Account"/>),
    accessor: 'Ledger',
    Cell: row => (<ColumnCell value={row.value} />),
    headerStyle: headerComponentStyle,
    style: columnComponentStyle
  },{
    accessor: 'Amount',
    show: false
  }, {
    Header: props => <ColumnHeader value={`$${totalBalance}`}/>,
    accessor: 'Balance',
    Cell: row => (<ColumnCell value={`$${row.value}`} textAlign="right"/>),
    minWidth: 30,
    headerStyle: { ...headerComponentStyle, textAlign: 'right'},
    style: { ...columnComponentStyle, fontWeight: '500', textAlign: 'right'}
  }]
);

// Needed a way to specific text colours for certain conditions
// To do it the React Table way, need to pass styles in getTdProps
export const setRowColours = (row, column) => {
  if (row.Amount > 0) return '#098B8C';
  if (column.id === 'Date' || column.id === 'Ledger') return 'rgb(158, 158, 161)';
  return 'black';
};
