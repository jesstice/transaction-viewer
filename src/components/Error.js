import React from 'react';
import PropTypes from 'prop-types';

import styles from './error.module.css';

const Error = ({ statusCode }) => (
  <div className={styles.error}>Sorry, it looks like there was a {statusCode} error!</div>
);

Error.propTypes = {
  statusCode: PropTypes.number.isRequired
};

export default Error;