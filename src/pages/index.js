import { graphql } from 'gatsby';
import React from 'react'
import 'typeface-montserrat';

import reset from './reset.css';
import styles from './index.module.css';
import Header from '../components/Header';
import TransactionTable from '../components/Table';
import { calculateAndFormatTransactions } from '../utils/calculateTransactionData';
import Error from '../components/Error';

const HomePage = ({ data, errors }) => {
  if (data.error.id !== 'DummyError') {
    return <Error statusCode={data.error.statusCode}/>
  }
  // calculate balance and format data
  // some of data could probably be formatted in a transformer plugin
  const { transactions, totalBalance } = calculateAndFormatTransactions(data.allBankTransaction.edges);

  return(
    <div>
      <Header title="Transaction Viewer"/>
      <div className={styles.tableWrapper}>
        <TransactionTable
          rows={transactions}
          totalBalance={totalBalance}
        />
      </div>
    </div>
  );
};

export const query = graphql`
  query {
    allBankTransaction(
      sort: {
        fields: [Date], order: DESC
      }
    ){
      edges {
        node {
          id,
          Date,
          Company,
          Amount,
          Ledger
        }
      }
    },
    error {
      id,
      statusCode
    }
  }
`;

export default HomePage;