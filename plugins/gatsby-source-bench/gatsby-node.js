const getAllTransactions = require('./api.js');
const processTransaction = require('./processNode');

exports.sourceNodes = async ({ actions, createNodeId }, configOptions) => {
  const { createNode } = actions;
  let error = undefined;

  // Gatsby adds a configOption that's not needed for this plugin, delete it
  delete configOptions.plugins;

  // create a dummy transaction for error handling, as graphQL query will break if
  // nothing is passed
  let transactions = [{
      "Date": "2013-12-22",
      "Company": "SHAW CABLESYSTEMS CALGARY AB",
      "Amount": "-110.71",
      "Ledger": "Phone & Internet Expense"
  }];

  try {
    transactions = await getAllTransactions();
  } catch (err) {
    console.log('Error getting all transaction data', err);
    error = err;
  }

  if (error) {
    // Need some way to bubble up errors to handle in React
    const nodeData = processTransaction(error, `${Date.now()}`, 'Error');
    createNode(nodeData);
  } else {
    const nodeData = processTransaction({ statusCode: 200 }, 'DummyError', 'Error');
    createNode(nodeData);
  }

  return transactions.forEach((transaction, index) => {
    const transactionId = createNodeId(`bank-transaction-${index}`);
    const nodeData = processTransaction(transaction, transactionId, 'BankTransaction');
    createNode(nodeData);
  });
};