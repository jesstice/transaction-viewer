const crypto = require('crypto');

const processTransaction = (transaction, id, type) => {
  const nodeId = id;
  const nodeContent = JSON.stringify(transaction);
  const nodeContentDigest = crypto
    .createHash("md5")
    .update(nodeContent)
    .digest("hex");

  const nodeData = Object.assign({}, transaction, {
    id: nodeId,
    parent: null,
    children: [],
    internal: {
      type,
      content: nodeContent,
      contentDigest: nodeContentDigest,
    },
  });

  return nodeData;
};

module.exports = processTransaction;