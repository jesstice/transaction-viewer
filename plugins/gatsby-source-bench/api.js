const rp = require('request-promise');

const getAllTransactions = async () => {
  let final = [];
  let page = 1;
  let transactionsAvailable = true;

  while (transactionsAvailable) {
    let response;
    try {
      response = await makeSingleRequest(page);
      response = JSON.parse(response);
    } catch (err) {
      if (err.statusCode === 404 && final.length > 0) {
        break;
      }
      transactionsAvailable = false;
      throw err;
    }

    const { totalCount, transactions } = response;

    await final.push.apply(final, transactions);
    page++;

    // check if we have the correct number of transactions
    if (final.length === totalCount) transactionsAvailable = false;
  }

  return final;
};

const makeSingleRequest = async page => {
  const url =  `http://resttest.bench.co/transactions/${page}.json`;
  return rp(url);
};

module.exports = getAllTransactions;