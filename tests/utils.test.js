import {
  calculateBalance,
  calculateAndFormatTransactions
} from '../src/utils/calculateTransactionData';
import { addCommaToNumber } from '../src/utils/addCommaToNumber';
import { fixture1 } from './fixtures';

describe('calculateAndFormatTransactions', () => {
  it('should add the previous transaction amount with the current amount', () => {
    const data = fixture1;
    const result = calculateAndFormatTransactions(data);
    const { transactions } = result;

    expect(transactions).toHaveLength(3);
    expect(parseFloat(transactions[0].Balance) - parseFloat(transactions[1].Balance))
      .toEqual(parseFloat(transactions[0].Amount));
  });
});

describe('calculateBalance', () => {
  it('should return a float with 2 decimal places', () => {
    const total = 130.55;
    const amount = -9.44;
    const result = calculateBalance(total, amount);

    expect(result).toBe(121.11);
  });
});

describe('addCommaToNumber', () => {
  it('should add commas to number and return a string', () => {
    const number = 13000.14;
    const result = addCommaToNumber(number);

    expect(result).toBe('13,000.14');
  });

  it('should should only return 2 decimal places', () => {
    const number = 1560.1400;
    const result = addCommaToNumber(number);

    expect(result).toBe('1,560.14');
  });
});