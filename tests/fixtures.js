export const fixture1 = [
  {
    "node": {
      "Date": "2013-12-22",
      "id": "95ab9689-89c7-5f5a-b8ed-b86a4506f667",
      "Company": "SHAW CABLESYSTEMS CALGARY AB",
      "Amount": "-110.71",
      "Ledger": "Phone & Internet Expense"
    }
  },
  {
    "node": {
      "Date": "2013-12-21",
      "id": "6ef12b39-4fa0-5dcf-b191-95ece3c5d88e",
      "Company": "BLACK TOP CABS VANCOUVER BC",
      "Amount": "-8.1",
      "Ledger": "Travel Expense, Nonlocal"
    }
  },
  {
    "node": {
      "Date": "2013-12-21",
      "id": "a90baaee-ca96-53bd-a21f-60b049040333",
      "Company": "GUILT & CO. VANCOUVER BC",
      "Amount": "-9.88",
      "Ledger": "Business Meals & Entertainment Expense"
    }
  }
];