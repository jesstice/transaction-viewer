# Transaction Viewer
A simple web application to view bank transactions that displays data from a Bench API and calculates the user's balance.

## Running in development
`gatsby develop`

## Testing
`npm test`

## Tools
#### Gatsby / React
Inspired after hearing positive things about Gatsby, it seemed like the perfect tool to accomplish this task. While it was fairly easy to work with, there were a few challenges. The main challenge working with Gatsby was understanding how to handle errors in the source plugins - this could probably be handled in a more graceful way.

#### React Table
Given the time constraints, I opted to choose a library that had good documentation and was well supported. However, the table was not intuitive to style and the library came with more features than required. In future iterations I would explore different options.

## Future Considersations
- Improve error handling (rather than using [dummy data](https://github.com/gatsbyjs/gatsby/issues/5296))
- Library for formatting numbers (like [Numeral](http://numeraljs.com/))
- Switch out React Table for another library or custom components